//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//
package org.dzingishan.killtheoverlord.characters;

import org.dzingishan.killtheoverlord.card.CharacterBase;
import org.dzingishan.killtheoverlord.game.ErrorCodes;
import org.dzingishan.killtheoverlord.game.GameProcess;
import org.dzingishan.killtheoverlord.game.Player;

/**
 * Can give coin and execution to player without using plot cards
 */
public class PeasantCharacter extends CharacterBase {

    public PeasantCharacter() {
        super("peasant", 9, 0, 2, true);
    }

    @Override
    public ErrorCodes executePassive(Player executor, Player receiver, GameProcess game) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ErrorCodes specialAction(int action, Object[] arguments) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
