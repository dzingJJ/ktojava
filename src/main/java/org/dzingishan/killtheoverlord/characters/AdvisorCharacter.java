//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//
package org.dzingishan.killtheoverlord.characters;

import org.dzingishan.killtheoverlord.card.CharacterBase;
import org.dzingishan.killtheoverlord.game.ErrorCodes;
import org.dzingishan.killtheoverlord.game.GameProcess;
import org.dzingishan.killtheoverlord.game.Player;

/**
 * When book is played, player can ignore it's effect
 */
public class AdvisorCharacter extends CharacterBase {

    public AdvisorCharacter() {
        super("Advisor", 1, 5, 7, true);
    }

    @Override
    public ErrorCodes executePassive(Player executor, Player receiver, GameProcess game) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ErrorCodes specialAction(int action, Object[] arguments) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
