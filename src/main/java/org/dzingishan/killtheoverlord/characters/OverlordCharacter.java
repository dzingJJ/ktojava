//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//
package org.dzingishan.killtheoverlord.characters;

import org.dzingishan.killtheoverlord.card.CharacterBase;
import org.dzingishan.killtheoverlord.game.ErrorCodes;
import org.dzingishan.killtheoverlord.game.GameProcess;
import org.dzingishan.killtheoverlord.game.Player;

/**
 * Gives execution to player
 */
public class OverlordCharacter extends CharacterBase {

    public OverlordCharacter() {
        super("overlord", 8, 6, 8, true);
    }

    @Override
    public ErrorCodes executePassive(Player executor, Player receiver, GameProcess game) { //TODO: can use it twice per game
        if (executor.equals(receiver)) {
            return ErrorCodes.INVALID_TARGET;
        }
        return game.moveExecution(executor, receiver);
    }

    @Override
    public ErrorCodes specialAction(int action, Object[] arguments) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
