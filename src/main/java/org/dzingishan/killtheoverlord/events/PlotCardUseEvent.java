//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//
package org.dzingishan.killtheoverlord.events;

import org.dzingishan.killtheoverlord.card.PlotCardBase;
import org.dzingishan.killtheoverlord.game.GameProcess;
import org.dzingishan.killtheoverlord.game.Player;

/**
 * Called when someone is using PlotCard
 */
public class PlotCardUseEvent extends Event {

    public final Player Executor, Receiver;
    public final GameProcess Process;
    public final PlotCardBase Card;

    public PlotCardUseEvent(Player executor, Player receiver, GameProcess process, PlotCardBase card) {
        super(false);
        Executor = executor;
        Receiver = receiver;
        Process = process;
        Card = card;
    }

}
