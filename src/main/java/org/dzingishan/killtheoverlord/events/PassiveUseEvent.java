//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//
package org.dzingishan.killtheoverlord.events;

import org.dzingishan.killtheoverlord.game.GameProcess;
import org.dzingishan.killtheoverlord.game.Player;

/**
 * Called when players uses passive
 */
public class PassiveUseEvent extends Event {

    public final Player Executor, Receiver;
    public final GameProcess Process;

    public PassiveUseEvent(Player executor, Player receiver, GameProcess process) {
        super(false);
        Executor = executor;
        Receiver = receiver;
        Process = process;
    }

}
