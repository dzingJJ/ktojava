/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dzingishan.killtheoverlord.events;

import org.dzingishan.killtheoverlord.card.CharacterBase;
import org.dzingishan.killtheoverlord.game.Player;

/**
 *
 * @author dzing
 */
public class CharacterChangeEvent extends Event {

    public final Player player;
    public final CharacterBase oldChara, newChara;

    public CharacterChangeEvent(Player player, CharacterBase oldChara, CharacterBase newChara) {
        super(false);
        this.player = player;
        this.oldChara = oldChara;
        this.newChara = newChara;
    }

}
