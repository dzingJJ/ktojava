/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dzingishan.killtheoverlord.events;

import org.dzingishan.killtheoverlord.game.GameProcess;

/**
 *
 * @author dzing
 */
public class GameStartEvent extends Event {

    public final GameProcess process;

    public GameStartEvent(GameProcess process) {
        super(false);
        this.process = process;
    }

}
