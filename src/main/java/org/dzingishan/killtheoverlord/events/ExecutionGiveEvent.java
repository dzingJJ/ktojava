//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//
package org.dzingishan.killtheoverlord.events;

import org.dzingishan.killtheoverlord.game.GameProcess;
import org.dzingishan.killtheoverlord.game.Player;

/**
 * Called when someone moving execution to other player
 */
public class ExecutionGiveEvent extends Event {

    public final Player From, To;
    public final GameProcess Process;

    public ExecutionGiveEvent(Player from, Player to, GameProcess process) {
        super(false);
        From = from;
        To = to;
        Process = process;
    }

}
