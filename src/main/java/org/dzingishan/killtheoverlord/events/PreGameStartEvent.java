//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//
package org.dzingishan.killtheoverlord.events;

import org.dzingishan.killtheoverlord.game.GameProcess;

/**
 * Called just before game start, before anything elsew
 */
public class PreGameStartEvent extends Event {

    public final GameProcess GameProcess;

    public PreGameStartEvent(GameProcess gameProcess) {
        super(false);
        GameProcess = gameProcess;
    }

}
