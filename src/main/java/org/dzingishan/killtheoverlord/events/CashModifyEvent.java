//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//
package org.dzingishan.killtheoverlord.events;

import org.dzingishan.killtheoverlord.game.GameProcess;
import org.dzingishan.killtheoverlord.game.Player;

/**
 * Called when Money is added to player
 */
public class CashModifyEvent extends Event {

    public final int FinalCoins;
    public final Player Player;
    public final GameProcess GameProcess;

    public CashModifyEvent(Player Player, int FinalCoins, GameProcess process) {
        super(false);
        this.GameProcess = process;
        this.FinalCoins = FinalCoins;
        this.Player = Player;
    }

}
