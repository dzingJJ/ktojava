//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//
package org.dzingishan.killtheoverlord.events;

import org.dzingishan.killtheoverlord.game.GameProcess;
import org.dzingishan.killtheoverlord.game.Player;

/**
 * Called when player dies
 */
public class WinEvent extends Event {

    public final Player Winner;
    public final GameProcess Process;

    public WinEvent(Player winner, GameProcess process) {
        super(false);
        Winner = winner;
        Process = process;
    }

}
