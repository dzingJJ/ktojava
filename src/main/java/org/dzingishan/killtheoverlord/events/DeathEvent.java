//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//
package org.dzingishan.killtheoverlord.events;

import org.dzingishan.killtheoverlord.game.GameProcess;
import org.dzingishan.killtheoverlord.game.Player;

/**
 * Called when player dies
 */
public class DeathEvent extends Event {

    public final Player Killer, Killed;
    public final GameProcess Process;

    public DeathEvent(Player killer, Player killed, GameProcess process) {
        super(false);
        Killer = killer;
        Killed = killed;
        Process = process;
    }

}
