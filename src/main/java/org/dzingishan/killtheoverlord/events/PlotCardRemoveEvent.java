/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dzingishan.killtheoverlord.events;

import org.dzingishan.killtheoverlord.card.PlotCardBase;
import org.dzingishan.killtheoverlord.game.GameProcess;
import org.dzingishan.killtheoverlord.game.Player;

/**
 *
 * @author dzing
 */
public class PlotCardRemoveEvent extends Event {

    public final GameProcess process;
    public final Player player;
    public final PlotCardBase card;

    public PlotCardRemoveEvent(GameProcess process, Player player, PlotCardBase card) {
        super(false);
        this.process = process;
        this.player = player;
        this.card = card;
    }
}
