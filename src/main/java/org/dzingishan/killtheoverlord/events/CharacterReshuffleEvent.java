//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//
package org.dzingishan.killtheoverlord.events;

import org.dzingishan.killtheoverlord.game.GameProcess;

/**
 * Called when characters are reshuffled. Random reshuffle is called one per
 * game - during game starting
 */
public class CharacterReshuffleEvent extends Event {

    public final boolean RandomReshuffle;
    public final GameProcess GameProcess;

    public CharacterReshuffleEvent(boolean RandomReshuffle, GameProcess GameProcess) {
        super(false);
        this.RandomReshuffle = RandomReshuffle;
        this.GameProcess = GameProcess;
    }

}
