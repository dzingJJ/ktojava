package org.dzingishan.killtheoverlord.events;

import org.dzingishan.killtheoverlord.card.PlotCardBase;
import org.dzingishan.killtheoverlord.game.GameProcess;
import org.dzingishan.killtheoverlord.game.Player;

/**
 * Called when game is adding plot card to the player
 */
public class PlotCardAddEvent extends Event {

    public final PlotCardBase Card;
    public final Player PlayerAdded;
    public final GameProcess Process;

    public PlotCardAddEvent(PlotCardBase card, Player playerAdded, GameProcess process) {
        super(false);
        Card = card;
        PlayerAdded = playerAdded;
        Process = process;
    }

}
