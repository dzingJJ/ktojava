//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//
package org.dzingishan.killtheoverlord.events;

/**
 * Base class for creating new Events
 */
public abstract class Event {

    /**
     * True if event is cancelled, otherwise false
     */
    public boolean Cancelled;
    /**
     * Can event be cancelled
     */
    public final boolean CanBeCancelled;

    public Event(boolean canBeCancelled) {
        CanBeCancelled = canBeCancelled;
    }

}
