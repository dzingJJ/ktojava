/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dzingishan.killtheoverlord.events;

import org.dzingishan.killtheoverlord.game.Player;

/**
 *
 * @author achim
 */
public class PlayerInteractionEvent extends Event {

    public PlayerInteractionEvent() {
        super(false);
    }

    public static enum Interaction {
        COERCION_INTERACTION,
        DISCARD_INTERACTION
    }

    public Player executor, receiver;
    public Interaction interaction;
}
