//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//
package org.dzingishan.killtheoverlord.game;

public enum Actions {
    /**
     * List of all possible actions connected with player
     *
     * Action of using plot card Arguments needed: args[0] = used plot card Some
     * cards may need other arguments
     */
    PLOT_CARD_USE,
    /**
     * Action of giving someone execution, NOT USING PLOT CARD No arguments
     * needed
     */
    //EXECUTION_GIVE,
    /**
     * Death of player
     */
    DEATH,
    /**
     * Action of giving money from bank, not from using plot card
     */
    INCOME,
    /**
     * Reshuffle of characters, not from
     */
    DRAFT,
    /**
     * Usage of character passive
     */
    PASSIVE
}
