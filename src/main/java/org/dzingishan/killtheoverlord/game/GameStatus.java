//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//
package org.dzingishan.killtheoverlord.game;

public enum GameStatus {
    /**
     * List of all possible game statuses
     *
     * Game Not Started Yet
     */
    NOT_STARTED,
    /**
     * Giving execution between players
     */
    EXECUTOR_GIVING,
    /**
     * Moment when people receive their money
     */
    INCOME,
    /**
     * Moment to give plot cards
     */
    DRAFT,
    /**
     * Moment to give character cards
     */
    CHARACTER_RESHUFFLE,
    /**
     * When character EXCEPT overlord is killed (See {@link #OVERLORD_KILLED}
     */
    CHARACTER_KILLED,
    /**
     * Only when overlord is killed
     */
    OVERLORD_KILLED,
    /**
     * When game is finished
     */
    END_OF_GAME
}
