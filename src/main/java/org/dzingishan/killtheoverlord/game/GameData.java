package org.dzingishan.killtheoverlord.game;

import java.util.ArrayList;
import java.util.List;
import static org.dzingishan.killtheoverlord.game.GameProcess.w;

/**
 *
 * @author dzing
 */
/// <summary>
/// Class contains current game data
/// </summary>
public class GameData {
    /// <summary>
    /// Unmodiffable list of players :-game
    /// </summary>

    public List<Player> Players = new ArrayList<Player>();

    /// <summary>
    /// Current game status
    /// </summary>
    public GameStatus GameStatus;

    public Player PlayerWithExecution;

    public boolean GameFinished;

    public GameData() {
    }

    public GameData(PlayerDetails[] players) {
        GameFinished = false;
        for (PlayerDetails play : players) {
            Players.add(new Player(play));
        }
    }

    public void DebugPrint() {
        w("GameData DATA");
        w("Players : game: " + Players.size());
        w("GameStatus: " + GameStatus);
        w("Execution is with player: " + PlayerWithExecution == null ? PlayerWithExecution.PlayerDetails.Nickname : "NOT YET");
        w("------PLAYERS-------");
//        Players.stream().map((p) -> {
//            p.Debug();
//            return p;
//        }).forEachOrdered((_item) -> {
//            w("---");
//        });
        w("------END PLAYERS------");

    }

    public Player GetPlayerByNumber(long uid) {
        for (Player player : Players) {
            if (player.PlayerDetails.UniqueNumber == uid) {
                return player;
            }
        }
        return null;
    }

    /// <summary>
    /// Gets player who is overlord
    /// </summary>
    /// <returns>Player with overlord card, if not present return null (should never happen)</returns>
    public Player GetOverlord() {
        for (Player pl : Players) {
            if (pl.CurrentCharacter.UniqueName.equals("overlord")) {
                return pl;
            }
        }
        return null;
        //return Players.stream().filter((o) -> o.CurrentCharacter.UniqueName.equals("overlord")).findFirst().get();
    }

    /// <summary>
    /// Checks if overlord is the only living player
    /// NOTE: If all players are dead, method will return true anyway
    /// </summary>
    /// <returns>True if all players are death, otherwise false</returns>
    public boolean OverlordOnlyLiving() {
        for (Player player : Players) {
            if (!player.CurrentCharacter.UniqueName.equals("overlord") && !player.Died) {
                return false;
            }
        }
        return true;
//        if (!Players.stream().noneMatch((player) -> (!player.Died && !player.CurrentCharacter.UniqueName.equals("overlord")))) {
//            return false;
//        }
//        return true;
    }
}
