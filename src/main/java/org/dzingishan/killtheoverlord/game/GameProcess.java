//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//
package org.dzingishan.killtheoverlord.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.dzingishan.killtheoverlord.card.CharacterBase;
import org.dzingishan.killtheoverlord.card.PlotCardBase;
import org.dzingishan.killtheoverlord.card.PlotCardType;
import org.dzingishan.killtheoverlord.events.CharacterReshuffleEvent;
import org.dzingishan.killtheoverlord.events.DeathEvent;
import org.dzingishan.killtheoverlord.events.ExecutionEndEvent;
import org.dzingishan.killtheoverlord.events.ExecutionGiveEvent;
import org.dzingishan.killtheoverlord.events.GameStartEvent;
import org.dzingishan.killtheoverlord.events.PassiveUseEvent;
import org.dzingishan.killtheoverlord.events.PlayerInteractionEvent;
import org.dzingishan.killtheoverlord.events.PlotCardUseEvent;
import org.dzingishan.killtheoverlord.events.PreGameStartEvent;
import org.dzingishan.killtheoverlord.events.RoundEndEvent;
import org.dzingishan.killtheoverlord.events.WinEvent;

/**
 * ONE MINUTE LIMIT TIME
 */
public class GameProcess {

    protected CardDatabase cardDatabase = new CardDatabase();
    protected EventsManager eventManager = new EventsManager();

    public static void w(Object o) {
        System.out.println(o);
    }

    // TODO | Catch last plot card used (retailiation)
    // TODO | Remember last player with execution
    // TODO | When execution hasn't been given yet, block other players moves
    public void revertLastAction() {
        // TODO
    }

    /**
     * Gets EventManager who handles in-game events
     *
     * @return
     */
    public EventsManager getEventManager() {
        return eventManager;
    }

    /**
     * Gets Cards Database with current cards
     *
     * @return
     */
    public CardDatabase getCardDatabase() {
        return cardDatabase;
    }

    public boolean gameStarted = true;

    protected GameData Game;
    private Map<DataKeys, Object> Data = new EnumMap<DataKeys, Object>(DataKeys.class);

    /**
     * Creates New GameProcess TODO: Checking number of characters
     *
     * @param players PlayerDetails to create players
     */
    public GameProcess(PlayerDetails[] players) {
        Game = (new GameData(players));
    }

    public GameProcess(GameData data) {
        Game = data;
    }

    public GameData getGame() {
        return Game;
    }

    /**
     * Tick of the game, some events uses it, you need to execute is twice per
     * second (500ms)
     */
    public void tick() {
    }

    /**
     * Gets data from Data dictionary Autocast type
     *
     * @param code {@link #DataKeys} to identify code
     * @param defaultValue Default value (it will be put in the array)
     * @return Object T type if found in dictionary, otherwise default
     */
    public <T> T getData(DataKeys code, T defaultValue) {
        if (Data.containsKey(code)) {
            return (T) Data.get(code);
        } else {
            Data.put(code, defaultValue);
            return (T) defaultValue;
        }
    }

    /**
     * Sets data to system
     *
     * @param code DataKey to set
     * @param value Value to set
     */
    protected void setData(DataKeys code, Object value) {
        Data.put(code, value);
    }

    public int getPlayerPosition(Player player) {
        for (int i = 0; i < Game.Players.size(); i++) {
            if (Game.Players.get(i).equals(player)) {
                return i;
            }
        }
        return -1;
    }

    public Player getPlayerOnPosition(int pos) {
        if (pos > Game.Players.size()) {
            pos -= Game.Players.size();
        } else if (pos < 0) {
            pos += Game.Players.size();
        }
        return Game.Players.get(pos);
    }

    /**
     * Starts Game and reshuffle characters
     */
    public void startGame() {
        gameStarted = true;
        /*
                     *  The begining of the game
                     *  Shuffle character cards
                     *  Give everybody 5 gold
                     *  Give everybody 4 cards
         */
        eventManager.callEvent(new PreGameStartEvent(this));
        setData(DataKeys.DEATH_ORDER, new ArrayList());
        giveOutCharactersRandom(null);
        //TODO: Debug option mayby?
        modifyCashOfAllPlayers(5);
        addPlotCardsToAllPlayers(4, true);
        getGame().PlayerWithExecution = getGame().GetOverlord();
        eventManager.callEvent(new GameStartEvent(this));
    }

    /**
     * Gets characters used in this game
     *
     * @return List of characters used in this game
     */
    public CharacterBase[] getCharactersPool() {
        CharacterBase[] arra = new CharacterBase[getGame().Players.size()];
        for (int i = 0; i < arra.length; i++) {
            arra[i] = getGame().Players.get(i).CurrentCharacter;
        }
        return arra;
    }

    /**
     * Adds cards to every player TODO: Some characters can have more then 4
     * cards. Workaround needed
     *
     * @param fourCards if true, add 4 cards to all players, else adds 2 cards
     * to max 4 cards
     */
    protected void addPlotCardsToAllPlayers(int cards, boolean force) {
        for (Player pl : getGame().Players) {
            if (!pl.Died) {
                pl.AddPlotCardsAsManyAsCan(cards, force, this);
            }
        }

//        getGame().Players.stream().filter((player) -> !(player.Died)).forEachOrdered((player) -> {
//            player.AddPlotCardsAsManyAsCan(cards, force, this);
//        });
    }

    /**
     * Modifies cash of all players
     *
     * @param gold How to modify cash
     */
    protected void modifyCashOfAllPlayers(int gold) {
        for (Player pl : getGame().Players) {
            pl.ModifyMoney(gold, this);
        }
//        getGame().Players.stream().forEach((p) -> {
//            p.ModifyMoney(gold, this);
//        });
    }

    /**
     * Trying to use given card
     *
     * @param executor Players who is using this card
     * @param receiver Players who is receiver of this card, can be null
     * @param usedCard Card used by executor
     * @return True if success, false and ErrorCode when failed
     */
    public ErrorCodes usePlotCard(Player executor, Player receiver, PlotCardBase usedCard) {
        if (getGame().GameFinished) {
            return (ErrorCodes.GAME_FINISHED);

        }

        if (!executor.HasCard(usedCard)) {
            return (ErrorCodes.PLAYER_DONT_HAVE_CARD);

        }

        if (usedCard.getCardType() == PlotCardType.BOOK && executor.HasCardOfGivenType(PlotCardType.SHIELD)) {
            return (ErrorCodes.BOOK_WHILE_HAVING_SHIELD);

        }
        if (usedCard.getCardType() != PlotCardType.SPECIAL && !Game.PlayerWithExecution.equals(executor)) {
            return ErrorCodes.PLAYER_DONT_HAVE_EXECUTION;
        } else if (usedCard.getUniqueName().equals("pleadguilty") && Game.PlayerWithExecution.equals(executor)) {
            return ErrorCodes.PLAYER_HAVE_EXECUTION;
        }
        ErrorCodes c = usedCard.useCard(executor, receiver, this);
        if (c == ErrorCodes.NO_ERROR) {
            eventManager.callEvent(new PlotCardUseEvent(executor, receiver, this, usedCard));
            executor.RemovePlotCard(usedCard, this);
        }
        return c;
    }

    /**
     * Executes passive from executer on receiver
     *
     * @param executor
     * @param receiver
     * @param error
     * @return
     */
    public ErrorCodes usePassive(Player executor, Player receiver) {
        if (getGame().GameFinished) {
            return (ErrorCodes.GAME_FINISHED);

        }

        ErrorCodes e = executor.CurrentCharacter.executePassive(executor, receiver, this);
        if (e == ErrorCodes.NO_ERROR) {
            eventManager.callEvent(new PassiveUseEvent(executor, receiver, this));
            executor.PassiveUsed = true;
        }

        return e;
    }

    public void registerWaitingEvent(PlayerInteractionEvent event) {
        // TODO
    }

    /**
     * Moves the execution order to given player and call the event
     *
     * @param from Player to bring execution from
     * @param to Player to give execution for
     * @return
     */
    public ErrorCodes moveExecution(Player from, Player to) {
        if (getGame().GameFinished) {
            return (ErrorCodes.GAME_FINISHED);

        }

        if (from.equals(to)) {
            return (ErrorCodes.EXECUTOR_EQUALS_TARGET);

        }

        eventManager.callEvent(new ExecutionGiveEvent(from, to, this));
        getGame().PlayerWithExecution = to;
        setData(DataKeys.LAST_EXECUTION_OWNER, from);
        return ErrorCodes.NO_ERROR;
    }

    public Player getPlayerByUUID(int player) {
        for (Player pl : Game.Players) {
            if (pl.getUniqueId() == player) {
                return pl;
            }
        }
        return null;
    }

    /**
     * Claim that players wants to die
     *
     * @param executor Executor of claim
     * @param error If returns false, error returned
     * @return True if players successfully died, else false
     */
    public ErrorCodes claimDeath(Player executor) {
        if (getGame().GameFinished) {
            return (ErrorCodes.GAME_FINISHED);

        }

        if (!getGame().PlayerWithExecution.equals(executor)) {
            return (ErrorCodes.PLAYER_DONT_HAVE_EXECUTION);

        }

        if (getData(DataKeys.EXECUTION_GIVEN, false)) {
            return (ErrorCodes.GIVE_FIRST_OVERLORD);

        }

        Player killer = (Player) getData(DataKeys.LAST_EXECUTION_OWNER, null);
        if (executor.CurrentCharacter.UniqueName.equals("overlord")) {
            // TODO | new Event (RoundEndEvent, ExecutionEndEvent)
            deathOverlord(executor, killer);
            return ErrorCodes.NO_ERROR;
        } else {
            deathNonOverlord(executor, killer);
            return ErrorCodes.NO_ERROR;
        }
    }

    private void deathNonOverlord(Player killed, Player killer) {
        /*
                     * After  death of nonoverlord:
                     * - give cash
                     * - check win
                     * - give 2 cards
         */
        killed.Died = true;
        eventManager.callEvent(new ExecutionEndEvent());
        startRound();
        ((List<Player>) getData(DataKeys.DEATH_ORDER, null)).add(killed);
        eventManager.callEvent(new DeathEvent(killer, killed, this));
    }

    public boolean isLowestRank(Player player) {
        List<Player> list = new ArrayList<>(Game.Players);
        list.sort(new Comparator<Player>() {
            @Override
            public int compare(Player t, Player t1) {
                return Integer.compare(t.CurrentCharacter.Rank, t.CurrentCharacter.Rank);
            }
        });
        return list.get(0).equals(player);
        //return Game.Players.stream().sorted(Comparator.comparing(o -> o.CurrentCharacter.Rank)).findFirst().orElse(null).equals(player);
    }

    public Player[] getPlayersSortedByRank() {
        List<Player> list = new ArrayList<>(Game.Players);
        list.sort(new Comparator<Player>() {
            @Override
            public int compare(Player t, Player t1) {
                return Integer.compare(t.CurrentCharacter.Rank, t.CurrentCharacter.Rank);
            }
        });
        return list.toArray(new Player[0]);
        //return Game.Players.stream().sorted(Comparator.comparing(o -> o.CurrentCharacter.Rank)).toArray(Player[]::new);
    }

    /**
     * Called when someone dies and game starts new round
     */
    private void startRound() {
        giveCashBasedOnCharacters();
        Player winner = checkWinCondition();
        if (winner != null) {
            eventManager.callEvent(new WinEvent(winner, this));
            getGame().GameFinished = true;
        } else {
            addPlotCardsToAllPlayers(2, false);
        }
    }

    /**
     * Called when overlord died
     *
     * @param killed Person killed
     * @param killer Killer
     */
    private void deathOverlord(Player killed, Player killer) {
        eventManager.callEvent(new DeathEvent(killer, killed, this));
        eventManager.callEvent(new RoundEndEvent());
        rearangeCharacters();
        resetDeath();
        startRound();
    }

    private void resetDeath() {
        for (Player pl : getGame().Players) {
            pl.Died = false;
        }
//        getGame().Players.forEach((pl) -> {
//            pl.Died = false;
//        });
        getData(DataKeys.DEATH_ORDER, (List) null).clear();
    }

    /**
     * Giving new characters to players
     */
    public void rearangeCharacters() {
        List<Player> died = getData(DataKeys.DEATH_ORDER, (List<Player>) null);
        List<Player> living = new ArrayList<Player>();
        for (Player p : Game.Players) {
            if (!p.Died) {
                living.add(p);
            }
        }
//        getGame().Players.stream().filter((p) -> (!p.Died)).forEachOrdered((p) -> {
//            living.add(p);
//        });
        CharacterBase[] Pool = getCharactersPool();
        living.sort(new Comparator<Player>() {
            @Override
            public int compare(Player t, Player t1) {
                return Integer.compare(t.CurrentCharacter.Rank, t.CurrentCharacter.Rank);
            }
        });
        Arrays.sort(Pool, new Comparator<CharacterBase>() {
            @Override
            public int compare(CharacterBase t, CharacterBase t1) {
                return Integer.compare(t.Rank, t1.Rank);
            }

        });
        //Player[] live = living.stream().sorted(Comparator.comparing(pl -> pl.CurrentCharacter.Rank)).toArray(Player[]::new);
        int i = 0;
        for (Player player : living) {
            player.SetCharacterCard(Pool[i++], this);
        }
        for (Player player : died) {
            player.SetCharacterCard(Pool[i++], this);
        }
    }

    /*
                 * And how now? 
                 * LIVING
                 * Biggest to Lowest
                 * DEATH:
                 * First To Last
     */
    /**
     * Checks if overlord have cash or overlord is only player lived
     *
     * {@code winner}
     *
     * @return True if someone wins, otherwise false
     */
    private Player checkWinCondition() {
        if (getGame().GetOverlord().Coins >= 30 || getGame().OverlordOnlyLiving()) {
            return getGame().GetOverlord();
        } else {
            return null;

        }
    }

    /**
     * Give cash to living people based on their characters
     */
    private void giveCashBasedOnCharacters() {
        for (Player pl : Game.Players) {
            if (!pl.Died) {
                pl.AddCoinsByCharacters(this);
            }
        }
//        getGame().Players.stream().filter((player) -> (!player.Died)).forEachOrdered((player) -> {
//            player.AddCoinsByCharacters(this);
//        });
    }

    /**
     * Gives random characters to players
     *
     * @param characters If you want to limit character pool, pass list here
     */
    public void giveOutCharactersRandom(CharacterBase[] characters) {
        List<CharacterBase> chars;
        if (characters != null) {
            chars = new ArrayList(Arrays.asList(characters));
        } else {
            chars = new ArrayList(Arrays.asList(cardDatabase.getCharactersBaseList()));
        }
        List<CharacterBase> cara = new ArrayList<>();
        //CharacterBase[] cara = new CharacterBase[getGame().Players.size()];
        Random ran = new Random();
        for (int i = 0; i < getGame().Players.size(); i++) {
            int n = ran.nextInt(chars.size());
            CharacterBase b = chars.get(n);
            cara.add(b);
            chars.remove(b);
        }
        if (!cara.contains(cardDatabase.getCardByUniqueID("overlord"))) {
            cara.set(ran.nextInt(cara.size()), cardDatabase.getCardByUniqueID("overlord"));
        }

        int j = 0;
        for (Player player : getGame().Players) {
            player.SetCharacterCard(cara.get(j++), this);
        }
        eventManager.callEvent(new CharacterReshuffleEvent(true, this));
    }

    /**
     * Gets how much of given card is on the deck
     *
     * @param cardBase Card to check
     * @return Numbers of cards
     */
    public int getCardsCount(PlotCardBase cardBase) {
        int a = 0;
        for (Player player : getGame().Players) {
            for (PlotCardBase b : player.PlotCards) {
                if (b.equals(cardBase)) {
                    a++;
                }
            }
            //a += (player.PlotCards.stream().filter(o -> o.equals(cardBase)).count());
        }
        return a;
    }

    /**
     * Outputs debug output on console
     */
    public void debugPrint() {
        w("DEBUG OUTPUT OF GAME");
        w("Game created: " + getGame() != null);
        w("---------------------");
        getGame().DebugPrint();
        w("---------------------");
    }

}
