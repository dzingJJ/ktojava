/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dzingishan.killtheoverlord.game;

import org.dzingishan.killtheoverlord.events.Event;

/**
 * Basic event listener class WARNING: DONT create second run method, it'll be
 * bad!
 *
 * @author dzing
 * @param <T> Classs of the EventListener
 */
public abstract class EventListener<T extends Event> {

    public abstract void run(T event);

}
