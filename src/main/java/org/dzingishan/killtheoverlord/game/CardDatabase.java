package org.dzingishan.killtheoverlord.game;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import org.dzingishan.killtheoverlord.card.CharacterBase;
import org.dzingishan.killtheoverlord.card.PlotCardBase;
import org.dzingishan.killtheoverlord.characters.GeneralCharacter;
import org.dzingishan.killtheoverlord.characters.OverlordCharacter;
import org.dzingishan.killtheoverlord.characters.PeasantCharacter;
import org.dzingishan.killtheoverlord.plotcards.CapitalizePlotCard;
import org.dzingishan.killtheoverlord.plotcards.CoercionPlotCard;
import org.dzingishan.killtheoverlord.plotcards.ConspirePlotCard;
import org.dzingishan.killtheoverlord.plotcards.ExtortionPlotCard;
import org.dzingishan.killtheoverlord.plotcards.MakeExamplePlotCard;
import org.dzingishan.killtheoverlord.plotcards.MartyrdomPlotCard;
import org.dzingishan.killtheoverlord.plotcards.PleadGuiltyPlotCard;
import org.dzingishan.killtheoverlord.plotcards.RevengePlotCard;
import org.dzingishan.killtheoverlord.plotcards.RevoltPlotCard;
import org.dzingishan.killtheoverlord.plotcards.RevolutionPlotCard;
import org.dzingishan.killtheoverlord.plotcards.SidestepPlotCard;
import org.dzingishan.killtheoverlord.plotcards.UprisingPlotCard;
import org.dzingishan.killtheoverlord.plotcards.VictimizePlotCard;

public class CardDatabase {

    public static final int PLOT_CARD_VERSION = 1, CHARACTER_VERSION = 1;

    private final Map<String, PlotCardBase> PLOT_CARDS = new HashMap<String, PlotCardBase>();
    private final Map<String, CharacterBase> CHARACTERS = new HashMap<String, CharacterBase>();
    private final Map<Integer, PlotCardBase> NUMBER_PLOT_CARD_LIST = new HashMap<Integer, PlotCardBase>();
    private final Random RANDOM_GENERATOR = new Random();

    {
        registerCharacterCard(new GeneralCharacter());
        registerCharacterCard(new OverlordCharacter());
        registerCharacterCard(new PeasantCharacter());

//        registerCharacterCard(new ConspirerCharacter());
//        registerCharacterCard(new KnightCharacter());
//        registerCharacterCard(new AdvisorCharacter());
//        registerCharacterCard(new CapitanCard());
//        registerCharacterCard(new GravediggerCharacter());
//        registerCharacterCard(new AnarchistCharacter());
//        registerCharacterCard(new ServantCharacter());
//        registerCharacterCard(new SquireCharacter());
        registerPlotCard(new MakeExamplePlotCard());
        registerPlotCard(new ConspirePlotCard());
        registerPlotCard(new CoercionPlotCard());
        registerPlotCard(new RevengePlotCard());
        registerPlotCard(new RevolutionPlotCard());
        registerPlotCard(new UprisingPlotCard());
        registerPlotCard(new VictimizePlotCard());
        registerPlotCard(new SidestepPlotCard());
        registerPlotCard(new ExtortionPlotCard());
        registerPlotCard(new CapitalizePlotCard());
        registerPlotCard(new PleadGuiltyPlotCard());
        registerPlotCard(new MartyrdomPlotCard());
        registerPlotCard(new RevoltPlotCard());
        System.out.println("Register " + Arrays.toString(getCharactersBaseList()));
        int j = 0;
        for (PlotCardBase b : PLOT_CARDS.values()) {
            for (int i = 0; i < b.getMaxCardCount(); i++) {
                NUMBER_PLOT_CARD_LIST.put(j++, b);
            }
        }
    }

    private int getPlotCardCount() {
        return NUMBER_PLOT_CARD_LIST.size();
    }

    public PlotCardBase[] getPlotCardsList() {
        return NUMBER_PLOT_CARD_LIST.values().toArray(new PlotCardBase[0]);
    }

    public CharacterBase[] getCharactersBaseList() {
        return CHARACTERS.values().toArray(new CharacterBase[0]);
    }

    /**
     * Registers CharacterBase card with it's unique id
     *
     * @param card
     */
    private void registerCharacterCard(CharacterBase card) {
        CHARACTERS.put(card.UniqueName, card);
    }

    /**
     * Registers Plot card with it's unique id
     *
     * @param card
     */
    private void registerPlotCard(PlotCardBase card) {
        PLOT_CARDS.put(card.getUniqueName(), card);
    }

    /**
     * Gets CharacterBase card by uuid
     *
     * @param uqid UUID of the card
     * @return Card found, otherwise null
     */
    public CharacterBase getCardByUniqueID(String uqid) {
        return CHARACTERS.get(uqid);
    }

    /**
     * Gets random Plot card
     *
     * @param process If Not null - checks maximum cards count
     * @return Random Plot Card, if not found, null
     */
    public PlotCardBase getRandomPlotCardNew(GameProcess process) {
        PlotCardBase ba = NUMBER_PLOT_CARD_LIST.get(RANDOM_GENERATOR.nextInt(NUMBER_PLOT_CARD_LIST.size()));
        if (process == null) {
            return ba;
        } else {
            int i = 0;
            do {
                if (process.getCardsCount(ba) >= ba.getMaxCardCount()) {
                    ba = NUMBER_PLOT_CARD_LIST.get(RANDOM_GENERATOR.nextInt(NUMBER_PLOT_CARD_LIST.size()));
                } else {
                    return ba;
                }
            } while (i++ < 10);
            return null;
        }
    }

    public PlotCardBase getCard(int card) {
        for (PlotCardBase pl : getPlotCardsList()) {
            if (pl.getUniqueId() == card) {
                return pl;
            }
        }
        return null;
    }

    public boolean isCharacter(Player player, String uniqueCardName) {
        if (player.CurrentCharacter.UniqueName.equals(uniqueCardName)) {
            return true;
        } else {
            return false;
        }
    }

}
