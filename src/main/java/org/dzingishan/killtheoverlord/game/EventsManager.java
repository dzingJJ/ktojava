/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dzingishan.killtheoverlord.game;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import org.dzingishan.killtheoverlord.events.Event;

/**
 *
 * @author dzing
 */
public class EventsManager {

    private Map<Class<? extends Event>, HashSet<EventListener>> map = new HashMap<>();

    private void registerEvent(Class<? extends Event> event) {
        if (!map.containsKey(event)) {
            map.put(event, new HashSet<EventListener>());
        }
    }

    public void registerEventListener(EventListener<? extends Event> e) {
        Class c = null;
        for (Method m : e.getClass().getDeclaredMethods()) {
            if (m.getName().equals("run") && m.getParameterTypes().length == 1) {
                c = m.getParameterTypes()[0];
                break;
            }
        }

        // Class c = Arrays.stream(e.getClass().getDeclaredMethods()).filter(o -> o.getName().equals("run") && o.getParameterTypes().length == 1).findFirst().get().getParameterTypes()[0];
        if (!map.containsKey(c)) {
            registerEvent(c);
        }
        map.get(c).add(e);
    }

    public void callEvent(Event event) {
        if (map.containsKey(Event.class)) {
            for (EventListener e : map.get(Event.class)) {
                e.run(event);
            }
            // map.get(Event.class).forEach(o -> o.run(event));
        }
        if (!map.containsKey(event.getClass())) {
            registerEvent(event.getClass());
        }
        for (EventListener l : map.get(event.getClass())) {
            l.run(event);
        }
        //map.get(event.getClass()).forEach(o -> o.run(event));
    }

}
