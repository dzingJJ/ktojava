//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//
package org.dzingishan.killtheoverlord.game;

/**
 * Stores Unique ID Of Player, used for identification of player in game
 *
 * @author dzing
 */
public class PlayerDetails {

    public String Nickname;
    public int UniqueNumber;

    public PlayerDetails(String nickname, int uniqueNumber) {
        Nickname = nickname;
        UniqueNumber = uniqueNumber;
    }

    public PlayerDetails() {
        this.Nickname = null;
        this.UniqueNumber = 0;
    }

    @Override
    public String toString() {
        try {
            return Nickname + ":" + UniqueNumber;
        } catch (RuntimeException __dummyCatchVar0) {
            throw __dummyCatchVar0;
        } catch (Exception __dummyCatchVar0) {
            throw new RuntimeException(__dummyCatchVar0);
        }

    }

}
