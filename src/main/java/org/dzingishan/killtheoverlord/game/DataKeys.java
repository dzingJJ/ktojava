//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//
package org.dzingishan.killtheoverlord.game;

public enum DataKeys {
    EXECUTION_GIVEN,
    LAST_EXECUTION_OWNER,
    DEATH_ORDER
}
