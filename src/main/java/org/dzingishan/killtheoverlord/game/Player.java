/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dzingishan.killtheoverlord.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.dzingishan.killtheoverlord.card.CharacterBase;
import org.dzingishan.killtheoverlord.card.PlotCardBase;
import org.dzingishan.killtheoverlord.card.PlotCardType;
import org.dzingishan.killtheoverlord.events.CashModifyEvent;
import org.dzingishan.killtheoverlord.events.CharacterChangeEvent;
import org.dzingishan.killtheoverlord.events.PlotCardAddEvent;
import org.dzingishan.killtheoverlord.events.PlotCardRemoveEvent;
import static org.dzingishan.killtheoverlord.game.GameProcess.w;

/**
 *
 * @author dzing
 */
public class Player {

    public PlayerDetails PlayerDetails;
    public int Coins;
    public boolean Died;

    public boolean PassiveUsed;
    public List<PlotCardBase> PlotCards;
    public CharacterBase CurrentCharacter;

    public Player(PlayerDetails play) {
        this.PlayerDetails = play;
        PlotCards = new ArrayList<PlotCardBase>();
    }

    /**
     * Sets current character card
     *
     * @param chara Character Card
     */
    public void SetCharacterCard(CharacterBase chara, GameProcess process) {
        process.eventManager.callEvent(new CharacterChangeEvent(this, CurrentCharacter, chara));
        this.CurrentCharacter = chara;
    }

    /**
     * Modify counts count
     *
     * @param coins How to modify coins
     */
    public void ModifyMoney(int coins, GameProcess process) {
        this.Coins += coins;
        process.eventManager.callEvent(new CashModifyEvent(this, this.Coins, process));
    }

    /**
     * Adds card to player
     *
     * @param b Card to add
     * @param process Current Game Process (needed for event call)
     */
    public void AddPlotCard(PlotCardBase b, GameProcess process) {
        process.eventManager.callEvent(new PlotCardAddEvent(b, this, process));
        PlotCards.add(b);
    }

    /**
     * Removes Plot card from player
     *
     * @param b Plot card to remove
     */
    protected void RemovePlotCard(PlotCardBase b, GameProcess process) {
        process.eventManager.callEvent(new PlotCardRemoveEvent(process, this, b));
        PlotCards.remove(b);
    }

    public void Debug() {
        w("PLAYER ");
        w("Player Data: " + PlayerDetails.toString());
        w("Current Card: " + (CurrentCharacter != null ? CurrentCharacter.UniqueName : "NOT YET"));
        w("Coins: " + Coins);
        w("PlotCards:\n-----------\n" + Arrays.toString(PlotCards.toArray()));
    }

    /**
     * Checks if player has card on hand
     *
     * @param card Card to check
     * @return True if player has this card, otherwise false
     */
    public boolean HasCard(PlotCardBase card) {
        return PlotCards.contains(card);
    }

    /**
     * Cheks if player has any card of given type
     *
     * @param type Type to check
     * @return True if have, false if not
     */
    public boolean HasCardOfGivenType(PlotCardType type) {
        for (PlotCardBase d : PlotCards) {
            if (d.getCardType().equals(type)) {
                return true;
            }
        }
        return false;
        //return PlotCards.stream().anyMatch((card) -> card.getCardType() == type);
    }
    /// <summary>
    /// Adds as many cards as can
    /// </summary>
    /// <param name="cards">How many cards try to add</param>
    /// <param name="force">If dont look on limit</param>

    /**
     * Adds as many random cards as can
     *
     * @param cards How mant cards try to add
     * @param force True if you want to force add cards, not seeing on limit
     * @param process Process (for Event)
     */
    public void AddPlotCardsAsManyAsCan(int cards, boolean force, GameProcess process) {
        int limit = GetMaxCardsCount();
        if (limit == 0) {
            limit = Integer.MAX_VALUE;
        }

        for (int i = 0; i < cards && (force || PlotCards.size() < limit); i++) {
            AddPlotCard(process.cardDatabase.getRandomPlotCardNew(process), process);
        }
    }

    /// <summary>
    /// Return  max card count
    /// </summary>
    /// <returns>0 if unlimited, otherwise</returns>
    /**
     * Return max plotcard limit
     *
     * @return 0 if unlimited, otherwise plotcard limit
     */
    public int GetMaxCardsCount() {
        return CurrentCharacter.PlotCardLimit;
    }

    /// <summary>
    /// Adds coins based on character
    /// </summary>
    /**
     * Adds coins based on current character
     *
     * @param process Process (For event)
     */
    void AddCoinsByCharacters(GameProcess process) {
        Coins += CurrentCharacter.GoldPerTurn;
        process.eventManager.callEvent(new CashModifyEvent(this, Coins, process));
    }

    /**
     * Gers player unique id
     *
     * @return
     */
    public int getUniqueId() {
        return PlayerDetails.UniqueNumber;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Player)) {
            return false;
        }
        return this.PlayerDetails.UniqueNumber == ((Player) obj).PlayerDetails.UniqueNumber;
    }

}
