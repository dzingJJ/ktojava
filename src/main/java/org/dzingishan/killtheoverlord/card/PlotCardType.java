//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:18
//
package org.dzingishan.killtheoverlord.card;

/**
 * Enum for card types
 *
 * @author dzing
 */
public enum PlotCardType {
    SHIELD,
    BOOK,
    SPECIAL
}
