//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:18
//
package org.dzingishan.killtheoverlord.card;

import org.dzingishan.killtheoverlord.game.ErrorCodes;
import org.dzingishan.killtheoverlord.game.GameProcess;
import org.dzingishan.killtheoverlord.game.Player;

/**
 * Base Class for all PlotCards
 *
 * @author dzing
 */
public abstract class PlotCardBase {

    private final String UniqueName;
    private final int UniqueId;
    private final PlotCardType CardType;
    private final int MaxCardCount;

    /**
     * Gets card uniqueid
     *
     * @return Uniqueid of card
     */
    public int getUniqueId() {
        return UniqueId;
    }

    /**
     * Type of card For more info see {@link PlotCardType}
     *
     * @return Type of card
     */
    public PlotCardType getCardType() {
        return CardType;
    }

    /**
     * How much cards of this type can be on the deck
     *
     * @return Max card limit
     */
    public int getMaxCardCount() {
        return MaxCardCount;
    }

    /**
     * Unique name of plot card
     *
     * @return
     */
    public String getUniqueName() {
        return UniqueName;
    }

    /**
     * Creates new PlotCardBase to register
     *
     * @param UniqueName Card unique name
     * @param UniqueId Unique id of card
     * @param CardType Card type
     * @param MaxCardCount Max number of cards on the deck
     */
    protected PlotCardBase(String UniqueName, int UniqueId, PlotCardType CardType, int MaxCardCount) {
        this.UniqueName = UniqueName;
        this.UniqueId = UniqueId;
        this.CardType = CardType;
        this.MaxCardCount = MaxCardCount;
    }

    /**
     * Called when you are using this card
     *
     * @param executor Who is executing
     * @param receiver Who is a receiver of card, can be null
     * @param game GameProcess, in which exchange took place
     * @return
     */
    public abstract ErrorCodes useCard(Player executor, Player receiver, GameProcess game);

    @Override
    public String toString() {
        try {
            return UniqueName + ":" + MaxCardCount + ":" + CardType;
        } catch (RuntimeException __dummyCatchVar0) {
            throw __dummyCatchVar0;
        } catch (Exception __dummyCatchVar0) {
            throw new RuntimeException(__dummyCatchVar0);
        }

    }

}
