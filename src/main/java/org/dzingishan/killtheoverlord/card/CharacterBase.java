//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:18
//
package org.dzingishan.killtheoverlord.card;

import org.dzingishan.killtheoverlord.game.ErrorCodes;
import org.dzingishan.killtheoverlord.game.GameProcess;
import org.dzingishan.killtheoverlord.game.Player;

/**
 * Base class for storing card data
 */
public abstract class CharacterBase {

    /**
     * Unique name of card, should be code name, localization is performed on
     * clientside
     */
    public final String UniqueName;
    /**
     * Unique card id
     */
    public final int uniqueNumber;
    /**
     * Determines how much gold must be given during budget round
     */
    public final int GoldPerTurn;
    /**
     * Determines how much health does character have (TODO?)
     */
    public final int Rank;
    /**
     * Max Plot Card Limit, Default Value is 4
     */
    public final int PlotCardLimit;
    /**
     * If true, Passive can be executed, otherwise passive is trigerred by other
     * event
     */
    public final boolean PassiveExecutable;

    /**
     * Creates new instance of characer base to register
     *
     * @param uniqueName Unique name of card, used for localization and
     * identification
     * @param uniqueid Unique id of the card
     * @param goldPerTurn How much gold per round he should receive, if this
     * characeter should lose gold every turn in favour of other player, please
     * use Events and set this to 0
     * @param rank Rank of the card
     * @param passiveExecutable Is his passive executable or is passive
     * @param plotCardLimit Max plot card limit, 0 for unlimited
     */
    protected CharacterBase(String uniqueName, int uniqueid, int goldPerTurn, int rank, boolean passiveExecutable, int plotCardLimit) {
        UniqueName = uniqueName;
        GoldPerTurn = goldPerTurn;
        Rank = rank;
        PlotCardLimit = plotCardLimit;
        PassiveExecutable = passiveExecutable;
        uniqueNumber = uniqueid;
    }

    /**
     * Createn new character to register with default plot card limit of 4
     *
     * @param uniqueName Unique name of card, used for localization and
     * identification
     * @param uniqueid Unique id of card
     * @param goldPerTurn How much gold per round he should receive, if this
     * characeter should lose gold every turn in favour of other player, please
     * use Events and set this to 0
     * @param rank Rank of the card
     * @param passiveExecutable Is his passive executable or is passive
     */
    protected CharacterBase(String uniqueName, int uniqueid, int goldPerTurn, int rank, boolean passiveExecutable) {
        UniqueName = uniqueName;
        GoldPerTurn = goldPerTurn;
        Rank = rank;
        PlotCardLimit = 4;
        PassiveExecutable = passiveExecutable;
        uniqueNumber = uniqueid;
    }

    /**
     * Executes Passive
     *
     * @param executor
     * @param receiver
     * @param game
     * @return
     */
    public abstract ErrorCodes executePassive(Player executor, Player receiver, GameProcess game);

    /**
     * Checks Special Actions
     *
     * @param action Special Action to check
     * @param arguments Arguments
     * @return Depends on implementation
     */
    public abstract ErrorCodes specialAction(int action, Object[] arguments);

}
