//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//
package org.dzingishan.killtheoverlord.plotcards;

import java.util.List;
import org.dzingishan.killtheoverlord.card.PlotCardBase;
import org.dzingishan.killtheoverlord.card.PlotCardType;
import org.dzingishan.killtheoverlord.game.ErrorCodes;
import org.dzingishan.killtheoverlord.game.GameProcess;
import org.dzingishan.killtheoverlord.game.Player;

/**
 * Give execution 1 or 2 to player on right, and draw one card
 */
public class SidestepPlotCard extends PlotCardBase {

    public SidestepPlotCard() {
        super("sidestep", 12, PlotCardType.SHIELD, 4);
    }

    @Override
    public ErrorCodes useCard(Player executor, Player receiver, GameProcess game) {
        if (executor.equals(receiver)) {
            return ErrorCodes.EXECUTOR_EQUALS_TARGET;
        } else {
            List<Player> players = game.getGame().Players;
            int nr = -1;

            for (int i = 0; i < players.size(); i++) {
                if (players.get(i).equals(executor)) {
                    nr = i;
                    break;
                }
            }
            if (!game.getCardDatabase().isCharacter(executor, "general") && !(receiver.equals(game.getPlayerOnPosition(nr + 1)) || receiver.equals(game.getPlayerOnPosition(nr + 2)))) {
                return ErrorCodes.INVALID_TARGET;
            }
        }
        executor.AddPlotCardsAsManyAsCan(1, true, game);
        return ErrorCodes.NO_ERROR;
    }
}
