//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//
package org.dzingishan.killtheoverlord.plotcards;

import org.dzingishan.killtheoverlord.card.PlotCardBase;
import org.dzingishan.killtheoverlord.card.PlotCardType;
import org.dzingishan.killtheoverlord.game.ErrorCodes;
import org.dzingishan.killtheoverlord.game.GameProcess;
import org.dzingishan.killtheoverlord.game.Player;

/**
 * Randomly gives characters and ends turn
 */
public class RevolutionPlotCard extends PlotCardBase {

    public RevolutionPlotCard() {
        super("revolution", 11, PlotCardType.BOOK, 1);
    }

    @Override
    public ErrorCodes useCard(Player executor, Player receiver, GameProcess game) {
        game.giveOutCharactersRandom(game.getCharactersPool());
        return ErrorCodes.NO_ERROR;
    }
}
