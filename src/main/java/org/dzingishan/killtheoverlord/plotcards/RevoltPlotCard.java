//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//
package org.dzingishan.killtheoverlord.plotcards;

import org.dzingishan.killtheoverlord.card.CharacterBase;
import org.dzingishan.killtheoverlord.card.PlotCardBase;
import org.dzingishan.killtheoverlord.card.PlotCardType;
import org.dzingishan.killtheoverlord.game.ErrorCodes;
import org.dzingishan.killtheoverlord.game.GameProcess;
import org.dzingishan.killtheoverlord.game.Player;

/**
 * Give Execution to player one rank bigger or less, and exchange characters
 * with him
 */
public class RevoltPlotCard extends PlotCardBase {

    public RevoltPlotCard() {
        super("revolt", 10, PlotCardType.SHIELD, 2);
    }

    @Override
    public ErrorCodes useCard(Player executor, Player receiver, GameProcess game) {
        if (executor.equals(receiver)) {
            return ErrorCodes.EXECUTOR_EQUALS_TARGET;
        } else {
            Player[] sortedPlayers = game.getPlayersSortedByRank();
            int nr = -1;

            for (int i = 0; i < sortedPlayers.length; i++) {
                if (sortedPlayers[i].equals(executor)) {
                    nr = i;
                    break;
                }
            }
            Player above = nr + 1 == sortedPlayers.length ? null : sortedPlayers[nr + 1];
            Player below = nr - 1 == -1 ? null : sortedPlayers[nr - 1];
            if (!game.getCardDatabase().isCharacter(executor, "general") && !(receiver.equals(above) || receiver.equals(below))) {
                return ErrorCodes.INVALID_TARGET;
            }
        }
        CharacterBase previousCharacterCard = executor.CurrentCharacter;
        executor.SetCharacterCard(receiver.CurrentCharacter, game);
        receiver.SetCharacterCard(previousCharacterCard, game);
        return ErrorCodes.NO_ERROR;
    }
}
