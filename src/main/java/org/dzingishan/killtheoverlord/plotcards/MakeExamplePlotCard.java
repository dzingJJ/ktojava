//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//
package org.dzingishan.killtheoverlord.plotcards;

import org.dzingishan.killtheoverlord.card.PlotCardBase;
import org.dzingishan.killtheoverlord.card.PlotCardType;
import org.dzingishan.killtheoverlord.game.ErrorCodes;
import org.dzingishan.killtheoverlord.game.GameProcess;
import org.dzingishan.killtheoverlord.game.Player;

/**
 *
 * Gives card only to player with lower rank, if you are lowest, you can give it
 * to anyone Bring one gold from banks
 */
public class MakeExamplePlotCard extends PlotCardBase {

    public MakeExamplePlotCard() {
        super("makeexample", 5, PlotCardType.SHIELD, 5);
    }

    @Override
    public ErrorCodes useCard(Player executor, Player receiver, GameProcess game) {
        if (executor.equals(receiver)) {
            return ErrorCodes.EXECUTOR_EQUALS_TARGET;
        } else {
            if (!game.getCardDatabase().isCharacter(executor, "general") && !(executor.CurrentCharacter.Rank > receiver.CurrentCharacter.Rank || game.isLowestRank(executor))) {
                return ErrorCodes.INVALID_TARGET;
            }
        }
        executor.ModifyMoney(1, game);
        game.moveExecution(executor, receiver);
        return ErrorCodes.NO_ERROR;
    }
}
