//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//
package org.dzingishan.killtheoverlord.plotcards;

import org.dzingishan.killtheoverlord.card.PlotCardBase;
import org.dzingishan.killtheoverlord.card.PlotCardType;
import org.dzingishan.killtheoverlord.game.ErrorCodes;
import org.dzingishan.killtheoverlord.game.GameProcess;
import org.dzingishan.killtheoverlord.game.Player;

/**
 * Kill yourself and after that player who gave you execution card
 */
public class RevengePlotCard extends PlotCardBase {

    public RevengePlotCard() {
        super("revenge", 9, PlotCardType.BOOK, 1);
    }

    @Override
    public ErrorCodes useCard(Player executor, Player receiver, GameProcess game) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
