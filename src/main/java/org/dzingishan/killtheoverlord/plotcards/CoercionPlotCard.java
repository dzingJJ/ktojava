//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//
package org.dzingishan.killtheoverlord.plotcards;

import org.dzingishan.killtheoverlord.card.PlotCardBase;
import org.dzingishan.killtheoverlord.card.PlotCardType;
import org.dzingishan.killtheoverlord.game.ErrorCodes;
import org.dzingishan.killtheoverlord.game.GameProcess;
import org.dzingishan.killtheoverlord.game.Player;

/**
 * Gives 1 player left or right Execution, he gives you plotcard or you bring 2
 * gold from bank
 */
public class CoercionPlotCard extends PlotCardBase {

    public CoercionPlotCard() {
        super("coercion", 2, PlotCardType.SHIELD, 3);
    }

    @Override
    public ErrorCodes useCard(Player executor, Player receiver, GameProcess game) {
        if (executor.equals(receiver)) {
            return ErrorCodes.EXECUTOR_EQUALS_TARGET;
        } else {
            int nr = game.getPlayerPosition(executor);
            if (game.getCardDatabase().isCharacter(executor, "general") && (receiver.equals(game.getPlayerOnPosition(nr + 1)) || receiver.equals(game.getPlayerOnPosition(nr - 1)))) {
                return ErrorCodes.INVALID_TARGET;
            }
        }
        game.moveExecution(executor, receiver);
        return ErrorCodes.NO_ERROR;
    }

}
