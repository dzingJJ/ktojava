//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//
package org.dzingishan.killtheoverlord.plotcards;

import org.dzingishan.killtheoverlord.card.PlotCardBase;
import org.dzingishan.killtheoverlord.card.PlotCardType;
import org.dzingishan.killtheoverlord.game.ErrorCodes;
import org.dzingishan.killtheoverlord.game.GameProcess;
import org.dzingishan.killtheoverlord.game.Player;

/**
 * Gives 1 person left right execution, rest people exchange their plot card
 */
public class ConspirePlotCard extends PlotCardBase {

    public ConspirePlotCard() {
        super("conspire", 3, PlotCardType.SHIELD, 3);
    }

    @Override
    public ErrorCodes useCard(Player executor, Player receiver, GameProcess game) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
