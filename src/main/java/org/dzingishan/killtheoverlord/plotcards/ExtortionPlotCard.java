//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//
package org.dzingishan.killtheoverlord.plotcards;

import org.dzingishan.killtheoverlord.card.PlotCardBase;
import org.dzingishan.killtheoverlord.card.PlotCardType;
import org.dzingishan.killtheoverlord.game.ErrorCodes;
import org.dzingishan.killtheoverlord.game.GameProcess;
import org.dzingishan.killtheoverlord.game.Player;

/**
 * Give Execution to any player, bring 1 gold from him (if can)
 */
public class ExtortionPlotCard extends PlotCardBase {

    public ExtortionPlotCard() {
        super("extortion", 4, PlotCardType.SHIELD, 4);
    }

    @Override
    public ErrorCodes useCard(Player executor, Player receiver, GameProcess game) {
        if (executor.equals(receiver)) {
            return ErrorCodes.EXECUTOR_EQUALS_TARGET;
        } else {
            if (!game.getCardDatabase().isCharacter(receiver, "knight")) {
                receiver.ModifyMoney(-1, game);
                executor.ModifyMoney(1, game);
            }
        }
        game.moveExecution(executor, receiver);
        return ErrorCodes.NO_ERROR;
    }
}
