//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//
package org.dzingishan.killtheoverlord.plotcards;

import org.dzingishan.killtheoverlord.card.PlotCardBase;
import org.dzingishan.killtheoverlord.card.PlotCardType;
import org.dzingishan.killtheoverlord.game.ErrorCodes;
import org.dzingishan.killtheoverlord.game.GameProcess;
import org.dzingishan.killtheoverlord.game.Player;

/**
 * Kills yourself and gets 7 gold from bank
 */
public class MartyrdomPlotCard extends PlotCardBase {

    public MartyrdomPlotCard() {
        super("martyrdom", 6, PlotCardType.BOOK, 1);
    }

    @Override
    public ErrorCodes useCard(Player executor, Player receiver, GameProcess game) {
        executor.ModifyMoney(7, game);
        game.claimDeath(executor);

        return ErrorCodes.NO_ERROR;
    }
}
