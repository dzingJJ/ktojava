/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dzingishan.killtheoverlord.plotcards;

import org.dzingishan.killtheoverlord.card.PlotCardBase;
import org.dzingishan.killtheoverlord.card.PlotCardType;
import org.dzingishan.killtheoverlord.game.ErrorCodes;
import org.dzingishan.killtheoverlord.game.GameProcess;
import org.dzingishan.killtheoverlord.game.Player;

/**
 *
 * @author dzing
 */
/**
 * Cancels last action
 */
public class RetaliatePlotCard extends PlotCardBase {

    public RetaliatePlotCard() {
        super("retaliate", 8, PlotCardType.SPECIAL, 4);
    }

    @Override
    public ErrorCodes useCard(Player executor, Player receiver, GameProcess game) {
        game.revertLastAction();
        return ErrorCodes.NO_ERROR;
    }
}
