//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//
package org.dzingishan.killtheoverlord.plotcards;

import org.dzingishan.killtheoverlord.card.PlotCardBase;
import org.dzingishan.killtheoverlord.card.PlotCardType;
import org.dzingishan.killtheoverlord.game.ErrorCodes;
import org.dzingishan.killtheoverlord.game.GameProcess;
import org.dzingishan.killtheoverlord.game.Player;

/**
 * Gives someone execution, if someone discards BOOK, he dies, otherwise
 * executor dies
 */
public class UprisingPlotCard extends PlotCardBase {

    public UprisingPlotCard() {
        super("uprising", 13, PlotCardType.BOOK, 3);
    }

    @Override
    public ErrorCodes useCard(Player executor, Player receiver, GameProcess game) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
