//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//
package org.dzingishan.killtheoverlord.plotcards;

import org.dzingishan.killtheoverlord.card.PlotCardBase;
import org.dzingishan.killtheoverlord.card.PlotCardType;
import org.dzingishan.killtheoverlord.game.ErrorCodes;
import org.dzingishan.killtheoverlord.game.GameProcess;
import org.dzingishan.killtheoverlord.game.Player;

/**
 * Pledge you are guilty, and brings plot card and 1 gold
 */
public class PleadGuiltyPlotCard extends PlotCardBase {

    public PleadGuiltyPlotCard() {
        super("pleadguilty", 7, PlotCardType.SPECIAL, 3);
    }

    @Override
    public ErrorCodes useCard(Player executor, Player receiver, GameProcess game) {
        game.moveExecution(game.getGame().PlayerWithExecution, executor);
        executor.AddPlotCardsAsManyAsCan(1, true, game);
        executor.ModifyMoney(1, game);
        return ErrorCodes.NO_ERROR;
    }
}
