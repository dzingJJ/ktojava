//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//
package org.dzingishan.killtheoverlord.plotcards;

import org.dzingishan.killtheoverlord.card.PlotCardBase;
import org.dzingishan.killtheoverlord.card.PlotCardType;
import org.dzingishan.killtheoverlord.game.ErrorCodes;
import org.dzingishan.killtheoverlord.game.GameProcess;
import org.dzingishan.killtheoverlord.game.Player;

/**
 * Give execution player 2 left/right, bring 2 gold from bank
 */
public class CapitalizePlotCard extends PlotCardBase {

    public CapitalizePlotCard() {
        super("capitalize", 1, PlotCardType.SHIELD, 4);
    }

    @Override
    public ErrorCodes useCard(Player executor, Player receiver, GameProcess game) {
        if (executor.equals(receiver)) {
            return ErrorCodes.EXECUTOR_EQUALS_TARGET;
        } else {
            int nr = game.getPlayerPosition(executor);
            if (!game.getCardDatabase().isCharacter(executor, "general") && (!game.getPlayerOnPosition(nr + 2).equals(receiver) || !game.getPlayerOnPosition(nr - 2).equals(receiver))) {
                return ErrorCodes.INVALID_TARGET;
            }
            game.moveExecution(executor, receiver);
            executor.ModifyMoney(2, game);
            return ErrorCodes.NO_ERROR;
        }
    }

}
