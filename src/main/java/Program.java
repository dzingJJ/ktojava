//
// Translated by CS2J (http://www.cs2j.com): 14.06.2017 08:00:19
//

import org.dzingishan.killtheoverlord.events.PreGameStartEvent;
import org.dzingishan.killtheoverlord.game.GameProcess;
import org.dzingishan.killtheoverlord.game.PlayerDetails;

public class Program {

    static void action(PreGameStartEvent evt) {
        System.out.println("EVT");
    }

    static boolean isInside(int x, int y, int r) {
        return x * x + y * y <= r * r;
    }

//    public static void main(String[] args)  {
//        Program.Main(args);
//    }
    public static void main(String[] args) {
        GameProcess process = new GameProcess(new PlayerDetails[]{new PlayerDetails("dz", 123124), new PlayerDetails("lol", 1234), new PlayerDetails("D", 12345)});
        process.startGame();

        //process.rearangeCharacters();
        //process.debugPrint();
    }

}
